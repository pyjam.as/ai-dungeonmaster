#!/usr/bin/env python3

# stdlib
from io import BytesIO
import random
import os
import asyncio

# deps
from requests import get, post
from pydub import AudioSegment, effects

# deps
from yate.ivr import YateIVR

# TODO: environment variables
PARROT_URL = os.getenv('PARROT_URL')
assert PARROT_URL
AWS_ACCESSKEY = os.getenv('AWS_ACCESSKEY')
assert AWS_ACCESSKEY
AWS_SECRETKEY = os.getenv('AWS_SECRETKEY')
assert AWS_SECRETKEY

TESTTEXT = "You enter a dungeon with your trusty sword and shield. You are searching for the evil necromancer who killed your family. You've heard that he resides at the bottom of the dungeon, guarded by legions of the undead. You enter the first door and see his body slumbering in a silk coffin. Siding towards you, you consult Sylvanas and warn her. She says visitors should 'earn your trust' as well as acknowledge you in return. But against your withering and unchanging will, the Spellbreaker grows entrapped in her crypt.  Options: 0) You attack him flatly.  1) You use your Shieldmaze to take down his fleshy body.  2) You tell Herbie, 'Pay the riddling.  3) You go to this open chamber where you find Sivir sitting near his flat, resolutely stilled.  Which action do you choose? "


def speak(speechpath):
    # get stuff from parrot
    pass


def get_speech(text):
    """ Generate speech using the parrot api """
    data = {"text": text,
            "voice": "Matthew",
            "aws_accesskey": AWS_ACCESSKEY,
            "aws_secretkey": AWS_SECRETKEY,
            "speed": 100}

    # generate speech
    speechid = post(PARROT_URL, json=data).text

    # download speech
    mp3bytes = get(f"{PARROT_URL}speech/{speechid}").content

    # make audio segment
    audioseg = AudioSegment.from_file(BytesIO(mp3bytes), format='mp3')
    audioseg = audioseg.set_frame_rate(8000)

    # export to PCM slin file, random filename
    filename = "adventure" + str(random.randint(100000, 999999)) + '.slin'
    audiopath = os.path.join('/tmp/', filename)
    audioseg.export(audiopath, format='s16le', parameters=["-filter:a", "volume=5dB"])
    return (audiopath, len(audioseg)/1000)
