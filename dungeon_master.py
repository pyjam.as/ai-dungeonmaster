#!/usr/bin/env python3

import os 
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# std lib
from generator import StoryGenerator
from story_tracker import StoryTracker
import tensorflow as tf
from utils import *
import asyncio
import random
import sys
from datetime import datetime

tf.logging.set_verbosity(tf.logging.ERROR)
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

# deps
from yate.ivr import YateIVR
from speech import get_speech

"""
Story flow:
1. Initial prompt seeds first story block
2. With story block (plus past additions) generate action possibilities
3. Using selected action (plus past additions) generate new story block
4. Repeat from 2

AI DM file handles high level interface functionality.
StoryTracker keeps track of story and action blocks, it can return the action prompt and the story block prompt

Dev Notes:
- It seems anecdotally that a top_k of 40 actually is better. Would be good to experiment with it more though.


"""
async def main(ivr: YateIVR):
    with open("/log/pyout.txt", "w") as logf:
        sys.stdout = logf
        with tf.Session(graph=tf.Graph()) as sess:
            welcomespeech, welcomelen = get_speech("Welcome to the AI Adventure Hacker game, please wait while tensorflow starts.")
            starttime = datetime.now()
            await ivr.play_soundfile(welcomespeech)
            
            # Make error speech
            invalidspeech, invalidspeechlen = get_speech("Invalid option, try again")

            generator = StoryGenerator(sess)
            story_tracker = StoryTracker()
            
            possible_stories = story_tracker.get_possible_stories()
            choice = "hacker"
            story_tracker.select_story(choice)
            story_block = generator.generate_story_block(story_tracker.start_prompt)
            initialtext = story_tracker.start_prompt + story_block
            initspeech, storylen = get_speech(initialtext)
            await asyncio.sleep(welcomelen - (datetime.now() - starttime).seconds)
            starttime = datetime.now()
            await ivr.play_soundfile(initspeech)
            while(True):
                # Get action prompt and generate possible actions
                action_prompt = story_tracker.get_action_prompt(story_block)
                action_phrases = story_tracker.get_action_phrases()
                options = generator.generate_action_options(action_prompt, action_phrases)

                # Get sound file for all options
                optionstext = "Options: "     
                for i, option in enumerate(options):
                    optionstext += (str(i+1) + ". " + option + ". ")

                optionsspeech, optionlen = get_speech(optionstext)
                await asyncio.sleep(storylen - (datetime.now() - starttime).seconds)
                starttime = datetime.now()
                await ivr.play_soundfile(optionsspeech)
                    
                 
                # Get dtmf selection
                while True:
                    choice = await ivr.read_dtmf_symbols(1, timeout_s=60)
                    if choice and int(choice) in range(1,5):
                        break
                    else:
                        await ivr.play_soundfile(invalidspeech)

                chosen_action = options[int(choice)-1]
                    
                # Play the selected option (again) sound file we already have
                chosenspeech, chosenlen = get_speech(chosen_action)
                starttime = datetime.now()
                await ivr.play_soundfile(chosenspeech)

                
                # Get next story_block based on selected action
                story_prompt = story_tracker.get_story_prompt(chosen_action)
                story_block = generator.generate_story_block(story_prompt)

                # Play story
                nextstoryspeech, storylen = get_speech(story_block)
                await asyncio.sleep(chosenlen - (datetime.now() - starttime).seconds)
                starttime = datetime.now()
                await ivr.play_soundfile(nextstoryspeech)


if __name__ == '__main__':
    ivr = YateIVR()
    ivr.run(main)
